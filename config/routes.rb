Rails.application.routes.draw do
  namespace :admin do
    resources :application do
      collection do
        get :comments
      end
    end

    root "application#index"
  end

  devise_for :users

  resources :articles
  resources :articles do
    collection do
      get :get_top_5
    end

    resources :comments
  end

  mount Ckeditor::Engine => '/ckeditor'
  root "articles#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
