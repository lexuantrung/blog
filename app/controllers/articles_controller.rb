class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]

  # GET /articles
  # GET /articles.json
  def index
    # get params[:search]
    if params[:search]
      @articles = Article.search(params[:search])
    elsif params[:tags]
      @articles = Article.filter_tags(params[:tags])
    elsif params[:author]
      @articles = Article.created_by_auhor(params[:author])
    else
      @articles = Article.all.order("created_at desc")
    end

    if @articles != nil?
      @articles = @articles.paginate(page: params[:page], per_page: 5)
    end
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
    @comment = @article.comments.build

    # Couting views
    @article.couting

    respond_to do |format|
      format.html
      format.js {render layout: false} # Add this line to you respond_to block
    end
  end

  # GET /articles/new
  def new
    @article = Article.new
    @article.views = 1
    # Check a user is logged-in or not
    # False then return
    if current_user.nil?
      deny_access
    end
  end

  # GET /articles/1/edit
  def edit
    if current_user.nil?
      deny_access
    elsif current_user.id != @article.author.id
      respond_to do |format|
        format.html {redirect_to @article, notice: "You don't have permitsion to do this" }
      end
    end
  end

  # POST /articles
  # POST /articles.json
  def create

    # Check user has
    @article = Article.new(article_params)
    # get current user and set to article
    @article.author = current_user
    respond_to do |format|
      if @article.save
        format.html { redirect_to @article, notice: 'Article was successfully created.' }
        format.json { render :show, status: :created, location: @article }
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article.destroy
    respond_to do |format|
      format.html { redirect_to admin_application_index_url, notice: 'Article was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:title, :content, :attachment, :tag_names)
    end
end
