class CommentsController < ApplicationController
  before_action :set_article
  before_action :set_comment, only: [:update, :destroy]

  # Because comment_url is partial_view so can't access it on url
  # Due to render to article
  def index
    respond_to do |format|
      format.html { redirect_to @article, notice: '' }
    end
  end

  # POST /articles/1/comment
  def create
    # Check user is logging or not
    if current_user.nil?
      deny_access
    else
      @comment = @article.comments.build(comment_params)
      @comment.author = current_user
      respond_to do |format|
        if @comment.save
          format.html { redirect_to @article, notice: 'Comment has been created.' }
          format.js {}
          format.json { render :show, status: :created, location: @comment }
        else
          format.html { render :new }
          format.json { render json: @comment.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  def edit
    #code
  end

  # PATCH/PUT /articles/1/comment/1
  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to @article, notice: 'comment was successfully updated.' }
        format.js{} #this line use to respong for require ajax
        format.json { render :show, status: :ok, location: @comment }
      else
        format.html { render :edit }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @comment.destroy
    respond_to do |format|
      format.html {redirect_to comments_admin_application_index_url, notice: 'Comment was successfully destroyed.'}
      format.json { head :no_content }
    end
  end

  private
  def set_article
      @article = Article.find(params[:article_id])
  end

  def set_comment
    @comment = Comment.find(params[:id])
    #code
  end

  def comment_params
    params.require(:comment).permit(:text)
    #code
  end
end
