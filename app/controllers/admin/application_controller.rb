class Admin::ApplicationController < ApplicationController
  before_action :user_logged_in?
  before_action :set_articles
  before_action :set_comments

  # GET /articles
  def index
    @articles
  end

  # GET /comments
  def comments
    @comments
    #code
  end

  private

  def set_articles
    @articles = Article.where("author_id = ?", "#{current_user.id}")
  end

  def set_comments
    @comments = Comment.where("author_id = ?", "#{current_user.id}")
  end

  def user_logged_in?
    if current_user.nil?
      deny_access
    end
  end
end
