class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  #
  before_action :store_current_location

  # if user not logged in
  # store the current location
  # redirect to login form
  def deny_access
    store_current_location

    redirect_to new_user_session_path
  end

  # Ovveride the devise helper to store the current location.
  # We can redirect to it after login
  def store_current_location
    session[:previous_url] = request.fullpath unless request.fullpath =~ /\/users/
  end

  #
  def after_sign_in_path_for(resource)
    session[:previous_url] || root_path
  end
end
