class Article < ApplicationRecord
  mount_uploader :attachment, AttachmentUploader

  # linking articles to User
  belongs_to :author, class_name: 'User'

  # linking articles to comments
  has_many :comments, dependent: :destroy

  # linking articles to tags
  # UNIQUE this meaning is only unique tags will be retrieved for each article
  has_and_belongs_to_many :tags, uniq: true

  # defining virtual attribute
  attr_accessor :tag_names

  # Overwriting the tag_names method provided by the attr_accessor
  def tag_names=(names)
    @tag_names = names

    names.split.each do |name|
      self.tags << Tag.where(name: name).first_or_initialize
    end
  end

  # each user acces any article then increase 1
  def couting
    self.views ||= 0
    self.views += 1
    self.update_column(:views, self.views)
  end

  # Searching all records that title or content include "params[:search]"
  def self.search(search)
    Article.where("title LIKE ? OR content LIKE ?", "%#{search}%", "%#{search}%").order(:created_at)
  end

  # Searching all records were written by "params[:author]"
  def self.created_by_auhor(author)
    Article.where("author_id = ?", "#{author}").order(:created_at)
  end

  # Filter all records has name tags is "params[:tags]"
  def self.filter_tags(tags)
    tag = Tag.find_by_name(tags)
    # Check tag before return articles
    if tag.nil?
      nil
    else
      tag.articles.order("created_at desc")
    end
  end

  # get top 5 most popular posts
  def self.get_top_popular_posts
    Article.order("views DESC").limit(5)
  end

  def self.get_recent_posts
    Article.order("created_at DESC").limit(5)
  end

end
