$(document).ready(function() {
  $('.comment-edit').click(function(event) {
    /* Act on the event */
    // console.log($(this).attr('value'));
    var $this = $(this);
    var value = 'comment-' + $this.attr('value');

    // Call method to show or hide form after click
    showElementByIdName(value);
  });

  // Create new funtion to show or hide editing comment form
  function showElementByIdName(idName) {
    if ($('#' + idName).hasClass('hidden')) {
      $('#' + idName).removeClass('hidden');
    } else {
      $('#' + idName).addClass('hidden');
    }
  }


  showNotice();

  function showNotice(){
    var notice = $('#notice').text();
    var modal = '';
    if (notice != '') {
      modal = '<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
                    '<div class="modal-dialog" role="document">' +
                '<div class="modal-content">'+
                  '<div class="modal-header">'+
                    '<h5 class="modal-title" id="exampleModalLabel">Notice</h5>' +
                    '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
                      '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                  '</div>' +
                  '<div class="modal-body">' +
                    notice +
                  '</div>' +
                  '<div class="modal-footer">' +
                    '<button type="button" class="btn btn-secondary btn-success" data-dismiss="modal">OK</button>' +
                  '</div>' +
                  '</div>' +
              '</div>' +
            '</div>';
      $('body').append(modal);
      $('#myModal').modal();
    }
  }
});
