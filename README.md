# Pre-work - *Blogs*

**Blogs** is a Ruby on Rails blog application.

Submitted by: **Le Xuan Trung**

Time spent: **it's about 100h**

URL: **https://radiant-plains-90592.herokuapp.com/**

## User Stories

The following is a list of **required** features of the application you'll build for our Pre-work.

* [x] Users can create, edit, and delete Articles from the database using their Rails application.
* [x] User can create a new Article, formatted using the Markdown language.
* [x] User can see how long ago the Article was created.
* [x] There is one Article that introduces the App Creator with name & picture.
* [x] User see a search form on the Articles page.
* [x] When the User attempts to delete an Article there is an alert modal which asks the User to confirm deletion of that specific Article.
* [x] User can submit a search term to find Articles with titles or body containing search terms.
* [x] User can create, edit, and delete comments on an Article.
* [x] User can navigate the application with a responsive navbar.
      http://v4-alpha.getbootstrap.com/examples/navbar/.
* [x] User can Sign Up, Log In, & Sign Out of our application using the Devise gem.      
* [x] The navbar will be responsive as well as **smart**, showing the user logical options related to authentication.
* [x] Only users that are logged in can create Articles.
* [x] Users can only edit & delete their own Articles.
* [x] User can only edit & delete their own comments.


The following **optional** features are implemented:

* [x] User can see who created the Article.
* [x] User can see how many views an Article has.
* [x] User can see who created the Comment.
* [x] User can add "tags" to a Article, and filter Articles by tag.


The following **additional** features are implemented:

- [ ] List anything else that you can get done to improve the app functionality!
- this app use gem "Panigate" to paging through data.
- User can view the most popular articles
- User can view all articles were posted by other.


## Video Walkthrough

Here's a walkthrough of implemented user stories:

-View online:
https://gifyu.com/image/bhx5

Path
![Video Walkthrough](/public/uploads/tmp/gif/pre-work.gif)

GIF created with [LiceCap](http://www.cockos.com/licecap/).

## Notes

Describe any challenges encountered while building the app.
- When i run app on local, i don't have any problem. But when i deploy app on Heroku, i get some problems such as i can't load Ckeditor on Heroku while this is normal if i run on local.

- Because i don't understand about uploading file in Ruby, so when i upload any file from my computer, this file can't upload into server.

- As you know, Storing data that user upload on heroku is temporary. I try to save it into another host such as amazon. But i can't. In case of heroku delete file user uploaded(cover photo) when they create new a article, this article will use default photo.

## License

    Copyright [yyyy] [name of copyright owner]

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
